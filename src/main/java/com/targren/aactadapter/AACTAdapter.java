package com.targren.aactadapter;

import com.targren.aactadapter.compat.AAMarkerAPI;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = AACTAdapter.modId, name = AACTAdapter.name, version = AACTAdapter.version, acceptedMinecraftVersions = "[1.12,1.12.2]", dependencies = AACTAdapter.dependencies)
public class AACTAdapter {

    public static final String modId = "aactadapter";
    public static final String name = "AACTAdapter";
    public static final String version = "1.12.2-0.1.0";
    public static final String dependencies = "required-after:crafttweaker@[1.12-4.1.8,);required-after:antiqueatlas@[1.12.2-4.4.9,)";

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        System.out.println(name + " is loading!");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        if (Loader.isModLoaded("antiqueatlas") && Loader.isModLoaded("crafttweaker")){
            AAMarkerAPI.init();
        }
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    }

}
