package com.targren.aactadapter.compat;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;

import crafttweaker.api.world.IWorld;
import hunternif.mc.atlas.api.AtlasAPI;
import hunternif.mc.atlas.api.MarkerAPI;
import hunternif.mc.atlas.registry.MarkerType;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.AAMarkerAPI")
@ZenRegister

@ModOnly("antiqueatlas")
public class AAMarkerAPI {

    private static boolean init = false;
    private static MarkerAPI pMarkerAPI;

    public static void init(){
        if (!init){
            pMarkerAPI = AtlasAPI.getMarkerAPI();
            MinecraftForge.EVENT_BUS.register(new AAMarkerAPI());
            init = true;
        }
    }

    public void registerMarker(MarkerType type){
        //NOP. Not applicable to crafttweaker scripts
    }

    @ZenMethod
    private static AAMarker putMarker(IWorld w, boolean visibleAhead, int atlasID, String markerType, String label, int x, int z){
       return new AAMarker(pMarkerAPI.putMarker((World)w.getInternal(), visibleAhead, atlasID, markerType, label, x, z));
    }

    @ZenMethod
    public static AAMarker putGlobalMarker(IWorld w, boolean visibleAhead, String markerType, String label, int x, int z){
        return new AAMarker(pMarkerAPI.putGlobalMarker((World)w.getInternal(), visibleAhead, markerType, label, x, z));
    }

    //******** Override wrappers for double-precision coordinate values
    @ZenMethod
    public static AAMarker putMarker(IWorld w, boolean visibleAhead, int atlasID, String markerType, String label, double x, double z){
        return putMarker(w, visibleAhead, atlasID, markerType, label, (int)Math.round(x), (int)Math.round(z));
    }


    @ZenMethod
    public static AAMarker putGlobalMarker(IWorld w, boolean visibleAhead, String markerType, String label, double x, double z){
        return putGlobalMarker(w, visibleAhead, markerType, label, (int)Math.round(x), (int)Math.round(z));
    }


    //******** Same as above, but returns the added Marker ID directly as an int
    @ZenMethod
    private static int putMarkerI(IWorld w, boolean visibleAhead, int atlasID, String markerType, String label, int x, int z){
        return pMarkerAPI.putMarker((World)w.getInternal(), visibleAhead, atlasID, markerType, label, x, z).getId();
    }

    @ZenMethod
    public static int putGlobalMarkerI(IWorld w, boolean visibleAhead, String markerType, String label, int x, int z){
        return pMarkerAPI.putGlobalMarker((World)w.getInternal(), visibleAhead, markerType, label, x, z).getId();
    }

    @ZenMethod
    public static int putMarkerI(IWorld w, boolean visibleAhead, int atlasID, String markerType, String label, double x, double z){
        return putMarker(w, visibleAhead, atlasID, markerType, label, (int)Math.round(x), (int)Math.round(z)).getId();
    }

    @ZenMethod
    public static int putGlobalMarkerI(IWorld w, boolean visibleAhead, String markerType, String label, double x, double z){
        return putGlobalMarker(w, visibleAhead, markerType, label, (int)Math.round(x), (int)Math.round(z)).getId();
    }


    //******** Now with new, marker deleting action!
    @ZenMethod
    public static void deleteMarker(IWorld w, int atlasID, int markerID){
        pMarkerAPI.deleteMarker((World)w.getInternal(), atlasID, markerID);
    }

    @ZenMethod
    public static void deleteGlobalMarker(IWorld w, int markerID){
        pMarkerAPI.deleteGlobalMarker((World)w.getInternal(), markerID);
    }


}
