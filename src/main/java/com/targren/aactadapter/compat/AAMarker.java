package com.targren.aactadapter.compat;

import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import hunternif.mc.atlas.marker.Marker;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;


@ZenClass("mods.AAMarker")
@ZenRegister

@ModOnly("antiqueatlas")
public class AAMarker {

    private Marker pMarker;

    public AAMarker(Marker mk){
        pMarker = mk;
    }

    @ZenMethod
    public int getId(){
        return pMarker.getId();
    }

    @ZenMethod
    public String getType() {
        return pMarker.getType();
    }

    @ZenMethod
    public String getLabel() { return pMarker.getLabel(); }

    @ZenMethod
    public String getLocalizedLabel() {
        return pMarker.getLocalizedLabel();
    }

    @ZenMethod
    public int getDimension() {
        return pMarker.getDimension();
    }

    @ZenMethod
    public int getX() {
        return pMarker.getX();
    }

    @ZenMethod
    public int getZ() {
        return pMarker.getZ();
    }

    @ZenMethod
    public int getChunkX() {
        return pMarker.getChunkX() >> 4;
    }

    @ZenMethod
    public int getChunkZ() {
        return pMarker.getChunkZ() >> 4;
    }

    @ZenMethod
    public boolean isVisibleAhead() {
        return pMarker.isVisibleAhead();
    }

    @ZenMethod
    public boolean isGlobal() {
        return pMarker.isGlobal();
    }
}
